
cs=read.csv("cellsize.csv",header = TRUE) #chargement du tableau avec les info sur la taille des cellules. 2 mesures de taille: AVE = average, STD = standard deviation
nd=data.frame(cs$X,cs$Cell.Size..AVE.,cs$Cell.Size..STD.) #un dataframe avec les noms de lignées cellulaires et les 2 tailles associées à chaque type

dataset_allNorm=load("Protquan_allNorm.Rdata")
 #chargement du dataset


#Nettoyage des noms de colonnes du tableau matquan_LFQ_vsn.
colnames(matquan_LFQ_vsn) <- sub(".*intensity\\.", "", colnames(matquan_LFQ_vsn)) #nettoyage des noms des colonnes: suppression du motif "intensity."
colnames(matquan_LFQ_vsn) <- gsub("cell|\\.", "", colnames(matquan_LFQ_vsn)) #suppression des mots "cell" et "." 
colnames(matquan_LFQ_vsn)=toupper(colnames(matquan_LFQ_vsn)) #mise des noms de colonnes en majuscules


#Nettoyage des noms de colonnes du tableau cellsize
nd[,1]=gsub("-|\\.| ","",nd[,1]) #suppression des caractères "-", ".", et espace dans les valeurs de la première colonne de nd
nd[,1]=toupper(nd[,1]) #mise des noms en majuscules de la 1ère colonne


#Recherche des indices des noms de colonnes dans matquan_LFQ_vsn qui ne sont pas dans la première colonne de matrice nd
which(!(colnames(matquan_LFQ_vsn) %in% nd[,1])) #5 24 54


#Les noms de colonnes de matquan_LFQ_vsn qui ne sont pas dans nd
colnames(matquan_LFQ_vsn)[5] #"CAKI1"
colnames(matquan_LFQ_vsn)[54] #"U251MG"
colnames(matquan_LFQ_vsn)[24] # "MCF7ADRR"


#Recherche des indices de lignes ayant ces noms de cellules dans nd
which (nd[,1]=="CAKI") #23
which (nd[,1]=="U251") #12
which (nd[,1]=="NCIADRRES") #58


#Remplacement
colnames(matquan_LFQ_vsn)[5]=nd[23,1] 
colnames(matquan_LFQ_vsn)[54]=nd[12,1]
colnames(matquan_LFQ_vsn)[24]=nd[58,1]


#Recherche des noms des colonnes manquantes dans matquan_LFQ_vsn
which(!(nd[,1] %in%  colnames(matquan_LFQ_vsn))) # 5 15 20 

csgholami=nd[-c(5,15,20),] #Suppression de ces colonnes
 

#Vérifications des correspondances
which(!(colnames(matquan_LFQ_vsn) %in% csgholami[,1])) #5 24 54
which(!(csgholami[,1] %in%  colnames(matquan_LFQ_vsn))) #11 20 55


#Réarrangement
rownames(csgholami)=csgholami[,1]
csgholami=csgholami[colnames(matquan_LFQ_vsn),] 
tghol=t(matquan_LFQ_vsn) #transposé de matquan_LFQ_vsn
dim(tghol) # 57 lignes, 2947 colonnes


#Сalcul des p-values
corr_AVE= numeric(100)
pvave = numeric(100)

for (i in seq(1,2947)) {
  correlation_result <- cor.test(tghol[1:57,i], csgholami[,2])
  pvave[i] <- correlation_result$p.value
  corr_AVE[i] = correlation_result$estimate
}


#Ajout des colonnes au tableau de Gholami
matquan_LFQ_vsn$pv_AVE=pvave
matquan_LFQ_vsn$corr_AVE=corr_AVE


#Tableau des corrélations pour Gholami
Tab_corr_Ghol=data.frame(Ghol_AVE=matquan_LFQ_vsn$corr_AVE, Ghol_pv=matquan_LFQ_vsn$pv_AVE)
rownames(Tab_corr_Ghol)=rownames(matquan_LFQ_vsn)


#Transformation de prot_name en gene_name
mapping=read.table("Mapping_MouseHuman.txt",header=TRUE)
names(mapping)


#Ajout de la colonne gene_name qui correspond aux prot_name
correspondances = match(rownames(Tab_corr_Ghol), mapping$input_ID_human)

Tab_corr_Ghol = Tab_corr_Ghol %>%
  mutate(gene_human = mapping$name[correspondances])



