# Protéosize



## Sujet d’étude

Les cellules changent de taille à divers moments de leur cycle de vie. Ce changement peut être observé dans diverses situations, par exemple, lors de la croissance cellulaire en fonction des phases du cycle (G1,S,G2,M), ou lors de la réponse immunitaire en réponse  aux agents pathogènes ou processus pathologiques. 
Le processus remarquable qui n’est pas pris en compte pour certaines raisons, est la variation différenciée des quantités de protéines pendant l’augmentation de la taille cellulaire.Le changement des quantités protéiques est associé à la croissance de la membrane cellulaire et à l’augmentation du volume de cytoplasme. En revanche, il faut remarquer que la quantité d'ADN reste constante.

## Objectifs

Au cours de ce projet ,trois  questions seront abordées:

Tout d’abord, nous allons identifier, les protéines dont les quantités se trouvent en corrélation avec la taille des cellules dans l’ensemble des données protéomiques fournies. On parlera donc de “marqueurs de taille” (ou “cell size markers” en anglais).

Nous allons les caractériser pour comprendre la raison de leur corrélation avec la taille des cellules. En particulier, nous recherchons leur localisation subcellulaire pour identifier si des compartiments cellulaires sont plus représentés dans cette liste de protéines marqueurs de taille. 

Dans un deuxième temps, nous testerons si elles peuvent être utilisées pour une estimation des changements de taille cellulaire seulement à partir de données de protéomique globale. Nous l’appliquerons dans le contexte des cancers (basé sur les datasets publics) et dans le cas d’activation des cellules immunitaires (basé sur les données fournies par l’équipe).
Enfin, dans un dernier temps, nous explorerons des méthodes de normalisation, qui visent à corriger l’impact de la variation de taille cellulaire sur le protéome, notamment dans le contexte de l'activation des cellules immunitaires ou pour comparer des sous-populations d'adipocytes de différentes tailles chez les patients. Ces méthodes ont pour but de corriger les effets du changement de taille lors de l’analyse statistique.

## Contenu

Dossiers:

- Frejno
- Gholami
- Jones
- Scripts

Scripts R :

- frejno.R 
- gholami.R
- jones.R
- ortholog.R
- Analyse_corr.R

Fichier text:

Mapping_MouseHuman.txt

Les dossiers contiennent les datas de données de protéomiques, ainsi que les tableaux des lignés cellulaires et leur taille. Le dernier dossier "Scripts" contient un ensemble de scripts conçus pendant la réalisation du projet.

Les trois premiers scripts sont les scripts de chargement et transformation des différents datasets jusqu'à obtention des tableaux de corrélation.
Le script ortholog.R permet de faire la correspondance entre ID_protein , nom de gènes humains et orthologues chez la souris.On obtient ainsi un tableau comportant les Id_protein de tous les datasets utilisés et les gènes correspondants (humains et orthologues murins).
Analyse_corr.R permet de réaliser les différents traitements des résultats des test de corrélations, ainsi que les plots de visualisation.

Le fichier Mapping_MouseHuman.txt est le tableau obtenu à partir du script ortholog.R. 


## Pré-requis

- library(dplyr)
- library(ggplot2)
- library(gridExtra)
- library(readr)
- library(VennDiagram)
- library(ggvenn)
- library(ggrepel)
- library(tidyverse)
- library(orthogene)



## Utilisation

- Téléchargez les scripts R et les fichiers de données dans un même répertoire
- Lancez une session R et chargez les scripts
- Assurez-vous que les chemin d'accès aux fichiers sont corrects
- Exécutez le script pour obtenir les résultats de l'analyse
- Les résultats de l'analyse seront générés dans la session R et peuvent être exportés selon les besoins.

## Version

R version 4.3.1 (2023-06-16 ucrt)

## Auteurs

- Fatma BELHABIB
- Anastasiia STREKNEVA
- Ibtissamath YESSOUFOU

Encadré par Marie LOCARD-PAULET

